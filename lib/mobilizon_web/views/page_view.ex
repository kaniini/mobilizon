defmodule MobilizonWeb.PageView do
  @moduledoc """
  View for our webapp
  """
  use MobilizonWeb, :view
end
